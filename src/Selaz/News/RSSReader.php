<?php

namespace Selaz\News;

use XMLReader;

class RSSReader {
	
	protected $feed;

	public function __construct( string $feed = null ) {
		$http = new Query( $feed );
		$this->feed = $http->download();
		
		$this->parse();
	}
	
	public function parse() {
		$xml = new XMLReader();
		$xml->open( $this->feed );
		$i = 0;
		$items = [];
		$isParserActive = false;

		$simpleNodeTypes = array( 'title', 'description', 'link' );

		while ( $xml->read() ) {
			$nodeType = $xml->nodeType;

			if ( $nodeType != XMLReader::ELEMENT && $nodeType != XMLReader::END_ELEMENT ) {
				continue;
			} else if ( $xml->name == "item" ) {
				if ( $nodeType == XMLReader::END_ELEMENT && $isParserActive ) {
					$i++;
				}
				$isParserActive = ( $nodeType != XMLReader::END_ELEMENT );
			}

			if ( !$isParserActive || $nodeType == XMLReader::END_ELEMENT ) {
				continue;
			}

			$name = $xml->name;

			if ( in_array( $name, $simpleNodeTypes ) ) {
				$xml->read();
				$items[ $i ][ $name ] = $xml->value;
			}
		}

		$itemsList = [];
		foreach ( $items as $item ) {
			$itemsList[] = new Item($item);
		}
		
		return $itemsList;
	}

}