<?php

namespace Selaz\News;

class Item {
	protected $title;
	protected $description;
	protected $link;
	
	public function __construct( array $data ) {
		$this->setDescription($data['description']);
		$this->setTitle($data['title']);
		$this->setLink($data['link']);
	}

	public function getTitle() {
		return $this->title;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getLink() {
		return $this->link;
	}

	public function setTitle( string $title ) {
		$this->title = $title;
	}

	public function setDescription( string $description ) {
		$this->description = $description;
	}

	public function setLink( string $link ) {
		$this->link = $link;
	}


}