<?php

namespace Selaz\Weather;

abstract class AbstractWeather {
	
	protected $key;
	
	protected $temp;
	protected $preassure;
	protected $hummidity;
	protected $windspeed;
	protected $winddirect;
	protected $forecasttime;
	protected $message;
	protected $forecast;
	
	protected $lon;
	protected $lat;

	protected $debug;

	public function __construct( string $key, float $lon, float $lat, bool $debug = false ) {
		$this->key = $key;
		$this->lon = $lon;
		$this->lat = $lat;
		$this->debug = $debug;
		
		$this->loadCurrentWeather();
	}
	
	protected  abstract function loadCurrentWeather();
	
	protected  abstract function loadForecast();
	
	protected abstract function query();
	
	public function getTemp() {
		
	}
	
	public function getPreassure() {
		
	}
	
	public function getHummidity() {
		
	}
	
	public function getWindSpeed() {
		
	}
	
	public function getWindDirect() {
		
	}
	
	public function getForecastTime() {
		
	}
	
	public function getForecast() {
		
	}
	public function getMessage() {
		
	}
}
	