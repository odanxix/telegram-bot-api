<?php

namespace Selaz\Weather;

class Yandex extends AbstractWeather {
	
	const API_URL = 'https://api.weather.yandex.ru/v1/forecast?lat=%s&lon=%s&lang=ru&limit=3&hours=false&extra=true';
	
	protected function loadCurrentWeather() {
		$data = $this->query();
		$fact = $data->fact;
		
		$this->forecasttime = $fact->obs_time;
		$this->temp = $fact->temp;
		$this->message = $this->convertCondition( $fact->condition );
		$this->windspeed = $fact->wind_speed;
		$this->winddirect = $fact->wind_dir;
		$this->preassure = $fact->pressure_mm;
		$this->hummidity = $fact->humidity;
	}

	private function convertCondition( string $cond ) : string {
		$conds = [
			'clear' => 'ясно',
			'partly-cloudy' => 'малооблачно',
			'cloudy' => 'облачно с прояснениями',
			'overcast' => 'пасмурно',
			'partly-cloudy-and-light-rain' => 'небольшой дождь',
			'partly-cloudy-and-rain' => 'дождь',
			'overcast-and-rain' => 'сильный дождь',
			'overcast-thunderstorms-with-rain' => 'сильный дождь, гроза',
			'cloudy-and-light-rain' => 'небольшой дождь',
			'overcast-and-light-rain' => 'небольшой дождь',
			'cloudy-and-rain' => 'дождь',
			'overcast-and-wet-snow' => 'дождь со снегом',
			'partly-cloudy-and-light-snow' => 'небольшой снег',
			'partly-cloudy-and-snow' => 'снег',
			'overcast-and-snow' => 'снегопад',
			'cloudy-and-light-snow' => 'небольшой снег',
			'overcast-and-light-snow' => 'небольшой снег',
			'cloudy-and-snow' => 'снег',
		];
		
		return $conds[$cond] ?? '';
	}
	
	protected function loadForecast() {
		
	}

	protected function query() {
		$http = new \Selaz\Tools\Query( sprintf( self::API_URL, $this->lat, $this->lon ) );
		
		$http->set(CURLOPT_RETURNTRANSFER, true);
		$http->addHeader( 'X-Yandex-API-Key', $this->key );
		
		$http->query();
		
		if ($this->debug) {
			printf(">>> %s\n", $http->getUrl());
			printf("<<< %s\n", $http->getQueryResult());
		}
		
		$result = $http->getQueryResultFromJson(false);
		
		print_r($result);
		
		return $result;
	}
}